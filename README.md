## Runit init scripts for GNU/Linux

This repository contains the runit init scripts for generic GNU/Linux
distributions.

This work is based on Artix GNU/Linux's
[runit-artix](https://github.com/artix-linux/runit-artix). Patches to Artix
GNU/Linux's repo may also be applied here.

## Dependencies

- A POSIX shell
- A POSIX awk
- procps-ng (needs pkill -s0,1)
- runit
- OpenRC (optional)

### How to use it

To see enabled services for "current" runlevel:

    $ ls -l /run/runit/service

To see available runlevels (default and single, which just runs sulogin):

    $ ls -l /etc/runit/runsvdir

To enable and start a service into the "current" runlevel:

    # ln -s /etc/runit/sv/<service> /run/runit/service

To disable and remove a service:

    # rm -f /run/runit/service/<service>

To view status of all services for "current" runlevel:

    # sv status /run/runit/service/*

Feel free to send patches and contribute with improvements!

## Copyright

runit-scripts is free software licensed under the GNU General Public License v3.
Read the [LICENSE](LICENSE) and [COPYING](COPYING) files for more information.
